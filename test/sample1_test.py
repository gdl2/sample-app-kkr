import unittest
from selenium import webdriver
# import pytest
import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options


class DemoAllure(unittest.TestCase):
    
    def test_site_loads(self):
        self.launch_site()
        self.verify_element()

    @allure.step("Launch site")
    def launch_site(self):
        options = Options()
        options.headless = True
        options.add_argument('--no-sandbox')
        self.driver = webdriver.Chrome(options=options)
        url="http://sampleappkkr.unmeetings.org/"
        self.driver.get(url);


    @allure.step("Verify element")
    def verify_element(self):
        xpath="//b[contains(text(),'Cases of Corona Virus')]"
        element_found=""
        wait = WebDriverWait(self.driver, 2)
        wait.until(EC.visibility_of_element_located((By.XPATH,xpath)))
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()


